import React, { Component } from 'react';

class Card extends Component {
    constructor(props) {
        super(props);
    }

render() {

    let styleBox = {

            backgroundPosition: this.props.x + "px" + " " + this.props.y + "px",
            borderColor: this.props.color
            
        }
    
  return (
    <div className="card_shirt">
        {/* <div className="card_img" style={styleBox(this.props.card.x, this.props.card.y)}> */}
        <div className="card_img" style={styleBox}>
        </div>
    </div>
  );
 }
}
 
 export default Card;