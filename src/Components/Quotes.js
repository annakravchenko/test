import React, { Component } from 'react';
import './../Styles/Quotes.css';
import axios from 'axios';
import quotes from './../quotes';

class Quotes extends Component {
    // getInitialState() {
    //     return {
    //         quoteList: []
    //     }
    // }
    
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			isLoaded: false,
			quotesData: {quotes}.quotes.data,
			quotesLength: {quotes}.quotes.data.length,
			currQuote: "The initial quote will be blank."
		};
    this.getRandomQuote = this.getRandomQuote.bind(this);
    
    }
    
    componentDidMount() {
      
        console.log("var ",this.state.quotesData);

        console.log ("The array length is: " + this.state.quotesLength);
			
        this.getRandomQuote();
	}
	getRandomQuote() {
		
		var randomNum = Math.floor((Math.random() * this.state.quotesLength) );
        console.log(randomNum);
		this.setState({
			currQuote: this.state.quotesData[randomNum].quote
        });   
        console.log("current", this.state.quotesData[randomNum].quote)
        console.log("current", this.state.currQuote)
	}
  
	render() {
		return ( <div id="content">

                
                <div className="qoutes__block">
                  {/* <img className="quotes__img quotes__img_left" src={require('./../Images/quote_marks_open.png')} alt="quote"/> */}
                  <img className="quotes__img" src={require('./../Images/quotes.gif')} alt="quote"/>

                  
                  <div className="qoutes__text">
                  {this.state.currQuote.split('\n').map((item, i) => {
                            return <p key={i}>{item}</p>;
                        })
                        }
                    </div>
                    {/* <img className="quotes__img quotes__img_right" src={require('./../Images/quote_marks_close.png')} alt="quote"/> */}
                
                </div>
                {/* <button id="new-quote" onClick={this.getRandomQuote}>New Quote</button> */}
            </div> );
	}
}
export default Quotes;