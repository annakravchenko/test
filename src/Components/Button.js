import React, { Component } from 'react';
import cards from './../cards';
import Card from './Card';

class Deal extends Component {
    constructor(props) {
        super(props);
        
		this.state = {
			isLoaded: false,
			cardDeck: [...cards.items],
            cardsLength: cards.items.length,
            leftHand:[],
            rightHand:[],
            rendered: false,
            leftPairs: [],
            rightPairs: []
        };
    this.getRandomCard = this.getRandomCard.bind(this);
    this.getCards = this.getCards.bind(this);
    this.compareLeft = this.compareLeft.bind(this);
    this.compareRight = this.compareRight.bind(this);

    }

    getCards(){
        this.state.isLoaded= false;
        this.state.cardDeck= [...cards.items];
        this.state.cardsLength = cards.items.length;
        this.state.leftHand = [];
        this.state.rightHand = [];
        this.state.leftPairs = [];
        this.state.rightPairs = [];

        const n = 14; 
        let list =[], leftH =[], rightH =[],  deckT= this.state.cardDeck;
        
        for(var i = 0; i < n; i++) {
            
            let randomNum = this.getRandomCard(i, deckT.length);
            list.push(randomNum);
            const movingItem = deckT[randomNum]
            deckT.splice(randomNum, 1);
            if(i<7)
                leftH.push(movingItem);
            else
                rightH.push(movingItem);
        }

        let lengthT = deckT.length;

        this.setState({
            rendered: true,
            isLoaded: true,
            cardDeck: deckT,
            cardsLength: lengthT,
            leftHand: leftH,
            rightHand: rightH,
            leftPairs: [],
            rightPairs: []
          });
    }
    
    compareLeft(){

        let tmpFirstLetter ="", data = [],
            tmpLeft = [...this.state.leftHand],
            colors = ["red", "blue", "yellow"];
        let tobreak = false;
        tmpLeft.map((card,i) => {
            tobreak = false;
                
            tmpFirstLetter = card.data[0].card[0];
            tmpLeft.map((card1,j) => {
                if (!tobreak){
                    if(card1.data[0].card[0]===tmpFirstLetter&&i<j){
                        data = { "data": [i, j, colors[0]] } ;
                        tobreak = true;
                        this.state.leftPairs.push(data);
                        colors.splice(0,1);
                        
                        delete tmpLeft[i];
                        delete tmpLeft[j];
                    }   
                }
            }
        )}
        );
    }
    compareRight(){
        let tmpFirstLetter ="", data = [],
            tmpRight = [...this.state.rightHand],
            colors = ["red", "blue", "yellow"];
        let tobreak = false;
        tmpRight.map((card,i) => {
            tobreak = false;
    
            tmpFirstLetter = card.data[0].card[0];
            tmpRight.map((card1,j) => {
                if (!tobreak){
                    if(card1.data[0].card[0]===tmpFirstLetter&&i<j){
                        data = { "data": [i, j, colors[0]]} ;
                        tobreak = true;
                        this.state.rightPairs.push(data);
                        colors.splice(0,1);

                        delete tmpRight[i];
                        delete tmpRight[j];
                    }
                }  
             }
        )
        }
        );
    }
  
    getRandomCard(i,n) {
		var randomNum = Math.floor((Math.random() * n) );
        return randomNum;        
    }
    
  render() {

    this.compareLeft();
    this.compareRight();

    let  cardHandLeft=null, cardHandRight=null;
    
    if (this.state.rendered ) {
        cardHandLeft = (
            <div className="hand__block_left">
                <img src="https://i.imgur.com/NoDsEDb.png" alt="winner" className="winnerImg" style={{display: (this.state.rightPairs<this.state.leftPairs)?"block":"none"}}></img>
                {this.state.leftHand.map((card, index) => {
                    let color="";
                    this.state.leftPairs.map(pair => {
                        if(pair.data[0]===index || pair.data[1]===index){
                            color=pair.data[2];
                        }
                    })
                    return <Card key={index} card={card.data[0].card} x={card.data[0].x} y={card.data[0].y} color={color} winner={(this.state.rightPairs<this.state.leftPairs)?true:false}></Card>
                })}
            </div>
            );

        cardHandRight = (
            <div className="hand__block_right">
                <img src="https://i.imgur.com/NoDsEDb.png" alt="winner" className="winnerImg" style={{display: (this.state.rightPairs>this.state.leftPairs)?"block":"none"}}></img>
                {this.state.rightHand.map((card, index) => {
                    
                    let color="";
                    this.state.rightPairs.map(pair => {
                        if(pair.data[0]===index || pair.data[1]===index){
                            color=pair.data[2];
                        }
                    })
                    return <Card key={index} card={card.data[0].card} x={card.data[0].x} y={card.data[0].y} color={color}></Card>
                })}
            </div>
            );
    }
    return (   
        <div className="container">
            <div className="container__layout container__block">
                <div className="layout">
                    <div className="layout__centered "> 
                        <button id="new-deal" onClick={this.getCards} type="submit">Push me</button>
                        <div className="hand__block">
                            {cardHandLeft}
                            {cardHandRight}   
                        </div>
                    </div>
                </div> 
            </div> 
        </div>
    );
  }
}
export default Deal; 