import React from 'react';
import './Styles/App.css';
import Deal from './Components/Button';

function App() {
  return (
    <div className="App"> 
      <div id="content">
        <Deal></Deal>       
      </div>
    </div>
  );
}

export default App;